Function Connect {

    $user = $env:UserName
    $hostsPath = "C:/Users/$user/.ssh/config"
    $fileHosts = Get-Content $hostsPath | Sort-Object
    $hosts = @()

    Foreach ($h in $fileHosts) {
        $length = $h.Length
        if ($length -gt 3) {
            if ($h.substring(0, 4) -eq "Host") {
                $hostName = $h.substring($length - ($length - 5))
                $hosts += "$hostName"

            }
        }
    }

    Write-Host "================ Conectarse a un Host ================"
    
    $c = 1


    Foreach ($h in $hosts) {
        Write-Host "$c - $h"
 
        $c = $c + 1
    }
    Write-Host "q: Presione q para salir."

    $selection = Read-Host "Introduzca el numero de host al que quiere conectarse"

    if ($selection -eq "q") {
        return
    }
    else {
        $selectedHost = $hosts[$selection - 1]
        Write-Host "Conectandose a $selectedHost"
        $command = "ssh $selectedHost"

        Invoke-Expression $command
    } 
}

Export-ModuleMember -Function Connect